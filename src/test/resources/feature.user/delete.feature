Feature: Delete an user

  Background:
    * url back.zenityDesbugsUrl
    * configure ssl = true
#    * def JDD = call read ('classpath:request/jddDelete.json')


  @test
  Scenario: Delete user
    Given path '/users/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNzBkNDc0NjAtZDhlYS00NjY0LTg2OWEtZjk0MzYxYjNkNDFjIiwidXNlcm5hbWUiOiJodW50ZXJmMTFUZXN0IiwiZW1haWwiOiJodW50ZXIxMWZAZXhhbXBsZS5jb20iLCJyb2xlIjoiSFVOVEVSIn0sImlhdCI6MTYzMDMzMjY2MSwiZXhwIjoxNjMwNDE5MDYxfQ.K96iDR6hh1GvWLFee3F4r-rNEp4QXhXhr6vbQ2etZjk?uuid=7bd6e5b5-aa15-472b-9082-7467ac930caa'
#    And request JDD
    When method DELETE
    Then status 200