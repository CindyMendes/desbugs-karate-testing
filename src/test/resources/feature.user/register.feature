Feature: Registration of a new user

  Background:
    * url back.zenityDesbugsUrl
    * configure ssl = true
    * def JDD = call read ('classpath:request/jddRegister.json')


  @test
  Scenario: Register user hunter
    Given path '/hunters'
    And request JDD
    And header Content-Type = 'application/json'
    And method POST
    Then status 201
    * def username = response.createdUser
    And print "lala " + username

  @test
  Scenario: Register user admin
    Given path '/admins'
    And request JDD
    And header Content-Type = 'application/json'
    And method POST
    Then status 201