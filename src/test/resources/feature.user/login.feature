Feature: Login Test

  @test
  Scenario: Connection to Desbugs
    * configure driver = front.navigateur
    * def data = read('request/Data.json')
    Given driver front.zenityDesbugsUrl + '/login'
    And driver.maximize()
    And driver.input('#email', data.email)
    And driver.input('#outlined-adornment-password', data.password)
    When click('button[type=submit]')
    And retry(5, 10000).waitForUrl(front.zenityDesbugsUrl + 'dashboard')
    Then match driver.url == front.zenityDesbugsUrl + 'dashboard'