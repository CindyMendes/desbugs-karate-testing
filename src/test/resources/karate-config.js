function fn() {
  var env = karate.env; // get system property 'karate.env'
  if (!env) {
    env = 'integration';
  }
  var config = {
    front: {
        navigateur: {type: 'chrome'},
        zenityDesbugsUrl: 'https://desbugs-front-hunter-dev.vercel.app/',

    },
    back : {
        navigateur: {type: 'chrome'},
        zenityDesbugsUrl: 'https://int-desbugs-api.herokuapp.com/api/v1',
        identifiant: 'HunterDemo',
        motdepasse: 'HunterDemo1'
    }
  }
  if (env == 'release') {
    // customize
    // e.g. config.foo = 'bar';
    config.front.zenityDesbugsUrl = 'https://desbugs-front-hunter-dev.vercel.app/';
    config.front.identifiant = 'identifiant_release';
    config.front.motdepasse = 'mdp_release';
  }
  // don't waste time waiting for a connection or if servers don't respond within 5 seconds
    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 5000);
    karate.configure('report', { showLog: true, showAllSteps: false } );
    karate.log('environnement utilisé pour ce test :', env);
  return config;
}